%global srcname fs

Name:           python-%{srcname}
Version:        2.4.11
Release:        10%{?dist}
Summary:        Python's Filesystem abstraction layer

License:        MIT
URL:            https://pypi.org/project/fs/
Source0:        https://github.com/PyFilesystem/pyfilesystem2/archive/v%{version}/%{srcname}-%{version}.tar.gz
Patch0:         fix-for-py3.9-release.patch

BuildArch:      noarch
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

BuildRequires:  python3dist(appdirs)
BuildRequires:  python3dist(pytz)
BuildRequires:  python3dist(six)


%global _description %{expand:
Think of PyFilesystem's FS objects as the next logical step to Python's file
objects. In the same way that file objects abstract a single file, FS objects
abstract an entire filesystem.}

%description %_description

%package -n python3-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{srcname}}

%description -n python3-%{srcname} %_description

%prep
%autosetup -n pyfilesystem2-%{version} -p1

%build
%py3_build

%install
%py3_install

%check
# Disabled tests run

%files -n python3-%{srcname}
%license LICENSE
%doc README.md
%{python3_sitelib}/%{srcname}-*.egg-info/
%{python3_sitelib}/%{srcname}/

%changelog
* Tue Aug 10 2021 Mohan Boddu <mboddu@redhat.com> - 2.4.11-10
- Rebuilt for IMA sigs, glibc 2.34, aarch64 flags
  Related: rhbz#1991688

* Tue Apr 27 2021 Parag Nemade <pnemade AT redhat DOT com> - 2.4.11-9
- Resolves: rhbz#1945164 - Remove dependency on python-pytest-randomly
- disable tests

* Fri Apr 16 2021 Mohan Boddu <mboddu@redhat.com> - 2.4.11-8
- Rebuilt for RHEL 9 BETA on Apr 15th 2021. Related: rhbz#1947937

* Wed Jan 27 2021 Fedora Release Engineering <releng@fedoraproject.org> - 2.4.11-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_34_Mass_Rebuild

* Wed Jul 29 2020 Fedora Release Engineering <releng@fedoraproject.org> - 2.4.11-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_33_Mass_Rebuild

* Wed Jun 24 2020 Parag Nemade <pnemade AT redhat DOT com> - 2.4.11-5
- Add missing BR: python3-setuptools

* Mon Jun 01 2020 Parag Nemade <pnemade AT redhat DOT com> - 2.4.11-4
- Disable few tests temporary for now (rhbz#1820916, rhbz#1841708)

* Tue May 26 2020 Miro Hrončok <mhroncok@redhat.com> - 2.4.11-3
- Rebuilt for Python 3.9

* Mon Mar 30 2020 Parag Nemade <pnemade AT redhat DOT com> - 2.4.11-2
- enable tests and use upstream source tarball

* Mon Mar 30 2020 Parag Nemade <pnemade AT redhat DOT com> - 2.4.11-1
- Initial packaging

